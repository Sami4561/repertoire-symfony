<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221211143045 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE champ_autre_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE champ_autre (id INT NOT NULL, contact_id INT NOT NULL, libelle VARCHAR(255) NOT NULL, contenu TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FA5D0FA9E7A1254A ON champ_autre (contact_id)');
        $this->addSql('ALTER TABLE champ_autre ADD CONSTRAINT FK_FA5D0FA9E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE champ_autre_id_seq CASCADE');
        $this->addSql('ALTER TABLE champ_autre DROP CONSTRAINT FK_FA5D0FA9E7A1254A');
        $this->addSql('DROP TABLE champ_autre');
    }
}
