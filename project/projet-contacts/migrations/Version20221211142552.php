<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221211142552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE champ_autre_id_seq CASCADE');
        $this->addSql('ALTER TABLE champ_autre DROP CONSTRAINT fk_fa5d0fa9e7a1254a');
        $this->addSql('DROP TABLE champ_autre');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE champ_autre_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE champ_autre (id INT NOT NULL, contact_id INT NOT NULL, libelle VARCHAR(255) NOT NULL, contenu TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_fa5d0fa9e7a1254a ON champ_autre (contact_id)');
        $this->addSql('ALTER TABLE champ_autre ADD CONSTRAINT fk_fa5d0fa9e7a1254a FOREIGN KEY (contact_id) REFERENCES contact (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
