<?php

namespace App\Twig;

use App\Repository\GroupeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;
use Twig\Extension\DebugExtension;

class TwigGlobalSubscriber implements EventSubscriberInterface {

    /**
     * @var \Twig\Environment
     */
    private $twig;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $manager;

    public function __construct( Environment $twig, GroupeRepository $groupeRepository) {
        $this->twig    = $twig;
        //$this->twig->enableDebug();
        //$twig->addExtension(new DebugExtension());
        $this->manager = $groupeRepository;
    }

    public function injectGlobalVariables( ControllerEvent $event ) {
        $groupes = $this->manager->findAll();
        $this->twig->addGlobal( 'groupes', $groupes);
    }

    public static function getSubscribedEvents() {
        return [ KernelEvents::CONTROLLER =>  'injectGlobalVariables' ];
    }
}