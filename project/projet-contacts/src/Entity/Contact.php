<?php

namespace App\Entity;

use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $prenom = null;

    #[ORM\Column(length: 255)]
    private ?string $telephone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $photo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\OneToMany(mappedBy: 'contact', targetEntity: ChampAutre::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $champAutres;

    #[ORM\ManyToOne(inversedBy: 'contacts')]
    private ?Groupe $groupe = null;

    public function __construct()
    {
        $this->setPhoto("default-avatar.png");
        $this->champAutres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, ChampAutre>
     */
    public function getChampAutres(): Collection
    {
        return $this->champAutres;
    }

    public function addChampAutre(ChampAutre $champAutre): self
    {
        if (!$this->champAutres->contains($champAutre)) {
            $this->champAutres->add($champAutre);
            $champAutre->setContact($this);
        }

        return $this;
    }

    public function removeChampAutre(ChampAutre $champAutre): self
    {
        if ($this->champAutres->removeElement($champAutre)) {
            // set the owning side to null (unless already changed)
            if ($champAutre->getContact() === $this) {
                $champAutre->setContact(null);
            }
        }

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }




}
