--
-- PostgreSQL database dump
--

-- Dumped from database version 13.9
-- Dumped by pg_dump version 13.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: notify_messenger_messages(); Type: FUNCTION; Schema: public; Owner: symfony
--

CREATE FUNCTION public.notify_messenger_messages() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
            BEGIN
                PERFORM pg_notify('messenger_messages', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$;


ALTER FUNCTION public.notify_messenger_messages() OWNER TO symfony;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: champ_autre; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.champ_autre (
    id integer NOT NULL,
    contact_id integer NOT NULL,
    libelle character varying(255) NOT NULL,
    contenu text
);


ALTER TABLE public.champ_autre OWNER TO symfony;

--
-- Name: champ_autre_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.champ_autre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.champ_autre_id_seq OWNER TO symfony;

--
-- Name: contact; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.contact (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    prenom character varying(255) NOT NULL,
    telephone character varying(255) NOT NULL,
    photo character varying(255) DEFAULT NULL::character varying,
    email character varying(255) DEFAULT NULL::character varying,
    groupe_id integer
);


ALTER TABLE public.contact OWNER TO symfony;

--
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_id_seq OWNER TO symfony;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO symfony;

--
-- Name: groupe; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.groupe (
    id integer NOT NULL,
    nom character varying(255) NOT NULL
);


ALTER TABLE public.groupe OWNER TO symfony;

--
-- Name: groupe_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.groupe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groupe_id_seq OWNER TO symfony;

--
-- Name: messenger_messages; Type: TABLE; Schema: public; Owner: symfony
--

CREATE TABLE public.messenger_messages (
    id bigint NOT NULL,
    body text NOT NULL,
    headers text NOT NULL,
    queue_name character varying(190) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    available_at timestamp(0) without time zone NOT NULL,
    delivered_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.messenger_messages OWNER TO symfony;

--
-- Name: messenger_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: symfony
--

CREATE SEQUENCE public.messenger_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messenger_messages_id_seq OWNER TO symfony;

--
-- Name: messenger_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: symfony
--

ALTER SEQUENCE public.messenger_messages_id_seq OWNED BY public.messenger_messages.id;


--
-- Name: messenger_messages id; Type: DEFAULT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.messenger_messages ALTER COLUMN id SET DEFAULT nextval('public.messenger_messages_id_seq'::regclass);


--
-- Data for Name: champ_autre; Type: TABLE DATA; Schema: public; Owner: symfony
--

COPY public.champ_autre (id, contact_id, libelle, contenu) FROM stdin;
1	1	home adress	Avenue d'Italie
2	1	test	test
3	3	Adresse postale	Rue du Général Foch, Saint-Symphorien-d'Ozon
4	5	Description	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
5	5	Notes	Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\.


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: symfony
--

COPY public.contact (id, nom, prenom, telephone, photo, email, groupe_id) FROM stdin;
3	LEPETIT	Benoît	0612345671	image-6398f70927347.jpg	lepetit.benoit@free.fr	1
5	FERNANDEZ	Joris	0612345456	default-avatar.png	joris.fernandez@wanadoo.fr	\N
4	NICOLAS	Amandine	0612345678	image-1-6398f7f6326d9.jpg	amandine.nicolas@gmail.com	2
1	DUPONT	Jean	0612345679	image-2-6398fd64bfe52.jpg	dupont.jean@gmail.com	1
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: symfony
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20221209231942	2022-12-09 23:23:00	933
DoctrineMigrations\\Version20221209234734	2022-12-09 23:48:34	998
DoctrineMigrations\\Version20221210141505	2022-12-10 14:15:49	730
DoctrineMigrations\\Version20221211142552	2022-12-11 14:26:14	822
DoctrineMigrations\\Version20221211143045	2022-12-11 14:31:03	915
DoctrineMigrations\\Version20221212232326	2022-12-12 23:23:43	1183
DoctrineMigrations\\Version20221212232954	2022-12-12 23:30:15	914
\.


--
-- Data for Name: groupe; Type: TABLE DATA; Schema: public; Owner: symfony
--

COPY public.groupe (id, nom) FROM stdin;
1	Travail
2	Famille
3	Amis
\.


--
-- Data for Name: messenger_messages; Type: TABLE DATA; Schema: public; Owner: symfony
--

COPY public.messenger_messages (id, body, headers, queue_name, created_at, available_at, delivered_at) FROM stdin;
\.


--
-- Name: champ_autre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.champ_autre_id_seq', 5, true);


--
-- Name: contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.contact_id_seq', 5, true);


--
-- Name: groupe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.groupe_id_seq', 3, true);


--
-- Name: messenger_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: symfony
--

SELECT pg_catalog.setval('public.messenger_messages_id_seq', 1, false);


--
-- Name: champ_autre champ_autre_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.champ_autre
    ADD CONSTRAINT champ_autre_pkey PRIMARY KEY (id);


--
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: groupe groupe_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.groupe
    ADD CONSTRAINT groupe_pkey PRIMARY KEY (id);


--
-- Name: messenger_messages messenger_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.messenger_messages
    ADD CONSTRAINT messenger_messages_pkey PRIMARY KEY (id);


--
-- Name: idx_4c62e6387a45358c; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_4c62e6387a45358c ON public.contact USING btree (groupe_id);


--
-- Name: idx_75ea56e016ba31db; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_75ea56e016ba31db ON public.messenger_messages USING btree (delivered_at);


--
-- Name: idx_75ea56e0e3bd61ce; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_75ea56e0e3bd61ce ON public.messenger_messages USING btree (available_at);


--
-- Name: idx_75ea56e0fb7336f0; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_75ea56e0fb7336f0 ON public.messenger_messages USING btree (queue_name);


--
-- Name: idx_fa5d0fa9e7a1254a; Type: INDEX; Schema: public; Owner: symfony
--

CREATE INDEX idx_fa5d0fa9e7a1254a ON public.champ_autre USING btree (contact_id);


--
-- Name: messenger_messages notify_trigger; Type: TRIGGER; Schema: public; Owner: symfony
--

CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON public.messenger_messages FOR EACH ROW EXECUTE FUNCTION public.notify_messenger_messages();


--
-- Name: contact fk_4c62e6387a45358c; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT fk_4c62e6387a45358c FOREIGN KEY (groupe_id) REFERENCES public.groupe(id);


--
-- Name: champ_autre fk_fa5d0fa9e7a1254a; Type: FK CONSTRAINT; Schema: public; Owner: symfony
--

ALTER TABLE ONLY public.champ_autre
    ADD CONSTRAINT fk_fa5d0fa9e7a1254a FOREIGN KEY (contact_id) REFERENCES public.contact(id);


--
-- PostgreSQL database dump complete
--

